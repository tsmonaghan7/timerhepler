<?php


namespace App\Libraries\Timer;

class NewTimerHelper
{
    public function __construct()
    {
        $this->marks = Array();
        $this->mark('start'); 
        $this->marks['stop'] = 0;
        $this->total = 0;
    }

    public function stop()
    {
        $this->mark('stop'); 
        $this->total = $this->total();
        return $this->marks['stop'];
    }

    public function mark($name)
    {
        $name = preg_replace("|\s|", "_", $name);
        $this->marks[$name] = microtime(true);
        return $this->marks[$name];
    }

    public function diff_between($first, $second)
    {
        $time_diff = $this->marks[$second] - $this->marks[$first];
        return round($time_diff, 2);
    }

    public function total()
    {
        return $this->diff_between('start', 'stop');
    }

    public function timeline()
    {
        asort($this->marks);
        $counter = 0;
        $results = Array();
        foreach ($this->marks as $name => $time)
        {
            if ($counter > 0)
            {
                $diff = $this->diff_between($last_mark_name, $name);
                $results[$last_mark_name . "_to_" . $name] = $diff;
                
            }
            $last_mark_time = $time;
            $last_mark_name = $name;
            $counter ++;
        }

        $results['start_to_stop'] = $this->diff_between('start', 'stop');
        return $results;
    }

    public function timeline_html()
    {
        $data = $this->timeline();
        $html =  '<table class = "table table-sm table-striped"> ';
        $html .= '<tr><th>item</th><th>time</th></tr>';
        foreach ($data as $name => $time)
        {
            $html .= '<tr><td>' . $name . '</td><td>' . $time . '</td></tr>';
        }
        $html .= '</table>';
        return $html;
        
    }

   
}