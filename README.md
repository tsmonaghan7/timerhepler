# README #

TimerHelper 

### What is this repository for? ###

* Helps you find slow points in your app
* Version 0.1


### How do I get set up? ###

* put the TimerHelper.php file in your app
* Make sure you can access it from all scripts
* Make sure you can pass the object around throughout your script - like in a blade file 

### Usage ### 

```php

require ("./TimerHelper.php");

$t = new TimerHelper(); // The timer starts here! 

$t->mark("about to start flux capacitor"); 

// actually start the flux capacitor here 

$t->mark("started flux capacitor"); 

$t->mark('another point in your script'); 

$t->stop(); // the timer stops 

$t->marks; // see all your marks 

// output: 
[
     "start" => 1586987426.7045,
     "stop" => 1586987475.2724,
     "about_to_start_flux_capacitor" => 1586987446.326,
     "started_flux_capacitor" => 1586987455.196,
     "another_point_in_your_script" => 1586987464.3872,
]

$t->total;

// output: 
=> 4.01


$t->timeline(); // the whole timeline 

// output: 
[
     "start_to_about_to_start_flux_capacitor" => 19.62,
     "about_to_start_flux_capacitor_to_started_flux_capacitor" => 8.87,
     "started_flux_capacitor_to_another_point_in_your_script" => 9.19,
     "another_point_in_your_script_to_stop" => 10.89,
]



$t->timeline_html(); // a table you can output at the bottom of a page (call this in your footer?) 

```
```html
<table class = "table table-sm table-striped"> 
  <tr>
    <th>item</th>
    <th>time</th>
  </tr>
  
  <tr>
    <td>start_to_about_to_start_flux_capacitor</td>
    <td>19.62</td>
  </tr>
  <tr>
    <td>about_to_start_flux_capacitor_to_started_flux_capacitor</td>
    <td>8.87</td>
  </tr>
  
  <tr>
    <td>started_flux_capacitor_to_another_point_in_your_script</td>
    <td>9.19</td>
  </tr>
  <tr>
    <td>another_point_in_your_script_to_stop</td>
    <td>10.89</td>
  </tr>
</table>



```